<?php
/**
* Implementation of hook_rules_event_info().
* @ingroup rules
*/
function fbssc_rules_rules_event_info() {
  return array(
    'fbssc_after_save' => array(
      'label' => t('User comments on Facebook Status.'),
      'module' => 'FBSSC',
      'arguments' => array(
        'status_message' => array('type' => 'string', 'label' => t('The text of the message which has been commented upon.')),
        'comment_message' => array('type' => 'string', 'label' => t('The text of the comment.')),
        'status_user' => array('type' => 'user', 'label' => t('The user that posted the original status.')),
        'wall_user' => array('type' => 'user', 'label' => t('The user that owns the wall')),
        'comment_user' => array('type' => 'user', 'label' => t('The user that posted the comment.')),
        'comment_time' => array('type' => 'number', 'label' => t('The date/time of the comment.')), //For some reason, date type doesn't work....
        'cid' => array('type' => 'number', 'label' => t('The comment ID.')),
        'sid' => array('type' => 'number', 'label' => t('The status ID.')),
      ),
    ),
  );
}

